#!/usr/bin/env bash

lib="$SKROOT/lib"
echo "-> $lib"

function _usage {
cat<<EOF
  
  Create a new ssh key pair

  Usage: socks new [OPTIONS]

  OPTIONS           
  --name,   -n        name of SSH key
  --crypto, -c        cryptographic algothrim
  --desc,   -d        add a descriptive comment [OPTIONAL]
EOF

exit 0
}

# TODO: move this to its own function
function _error {
cat<<EOF

  SSH key name and crypto required!
  Run socks new --help for usage

EOF

exit 1
}

# TODO: find a better way of dealing with error messages
function new.key {
  # TODO: create and array of hash table for this?
  local name
  local crypto
  local desc
  shift
  
  while [[ $# > 0 ]]; do
    case "$1" in
      --help | -h)
        _usage
        ;;
      --name | -n)
      	if [[ -z "$2" ]]; then
      	  echo "Error: option \"--name\" requires a string"
      	  exit 1
      	fi
        name="$2"
        shift 2
        ;;
      --crypto | -c)
      	if [[ -z "$2" ]]; then
      	  echo "Error: option \"--crypto\" cannot be empty"
      	  exit 1
      	fi
        crypto="$2"
        shift 2
        ;;
      --desc | -d)
        if [[ -z "$2" ]]; then
          break
        fi
        desc="$2"
        shift 2
        ;;
      *)
        echo "Error: Invalid option \"$1\""
        echo "run socks new --help, for usage"
        exit 1
        ;;
    esac
  done

  if [[ ! -z $name ]] || [[ ! -z $crypto ]]; then
    source "$lib/generate_key.sh"
    echo "Generating key..."
    generate_key $name $crypto $desc
    exit 0
  else
    _error
  fi
}
