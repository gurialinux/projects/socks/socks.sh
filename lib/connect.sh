#!/usr/bin/env bash

# TODO: need to check if ssh is able to connect remote server
# TODO: need to figure out user friendly error messages
function connect {
  if [[ ! -z "$1" ]] || [[ ! -z "$2" ]]; then
    ssh "$1@$2"

    if [[ "$?" -eq 0 ]]; then
      return
    fi
  fi

  exit 1
}