#!/usr/bin/env bash

local dir="$HOME/.ssh"

function generate_key {
   if [[ -z "$3" ]]; then
    ssh-keygen -f "$dir/$1" -t "$2"
   else
    ssh-keygen -f "$dir/$1" -t "$2" -C "$3"
   fi

   # FIXME: remove this a run this function directly
   if [[ $! -eq 0 ]]; then
    echo "SSH $1 with created Ok!"
    exit 0
   fi

   #TODO: find a way to better deal with errors and error messages
   echo "Failed to generate $1 key"
   exit 1
}
