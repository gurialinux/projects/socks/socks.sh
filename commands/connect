#!/usr/bin/env bash

lib="$(dirname 0)/lib"

function usage {
	echo ""
	echo "USAGE: termite connect [OPTIONS]"
	echo "--username, -u       username of user on remote host"
	echo "--host, -H           ip addres of remote host"
	echo ""
}

function connect.key {
	local username
	local host
	shift

	while [[ $# > 0 ]]; do
		case "$1" in
			--help | -h)
				usage
				exit 0
				;;
			--username | -u)
				if [[ -z "$2" ]]; then
					echo "Error: option \"--username\" required"
					exit 1
				fi
				username="$2"
				shift 2
				;;
			--host | -H)
				if [[ -z "$2" ]]; then
					echo "Error: option \"--host\" required"
					exit 1
				fi
				host="$2"
				shift 2
				;;
			*)
				echo "Invalid option"
				usage
				exit 1
				;;
		esac
	done

	if [[ ! -z "$username" ]] || [[ ! -z "$host" ]]; then
		source "$lib/connect.sh"
		echo "Connecting to $host"
		connect $username $host
		exit 0
	fi

	usage
	exit 1
}
